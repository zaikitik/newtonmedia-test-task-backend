using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using TestTask.Models;

namespace TestTask.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class DriverData : Controller
    {
        private readonly ILogger _logger;
        private List<DriverProfile> _driverProfiles;

        
        private readonly Dictionary<int, string> _brandIdToNameMap = new ()
        {
            [0] = "BMW",
            [1] = "Audi",
            [2] = "Mercedes",
            [3] = "Skoda",
            [4] = "Fiat",
            [5] = "Renault",
            [6] = "Lexus",
            [7] = "Ferrari",
            [8] = "Porsche",
            [9] = "Kia",
        };

        
        public DriverData(ILogger<DriverData> logger)
        {
            _logger = logger;
        }
        
        [HttpPost]
        public DriverDataResponse CalculateStatistics([FromBody] List<DriverProfile> driverProfiles)
        {
            _driverProfiles = driverProfiles;

            DriverDataResponse response = GenerateResponse();

            return response;
        }

        private DriverDataResponse GenerateResponse()
        {
            var response = new DriverDataResponse();
            
            Parallel.Invoke(
                () => response.AverageAge = CalculateAverageAge(),
                () => response.DriverCountByYearOfBirth = CalculateDriverCountByYearOfBirth(),
                () => response.DriverIdsByLastNames = CalculateDriverIdsByLastNames(),
                () => response.MostFrequentlyUsedBrands = CalculateMostFrequentlyUsedBrands(),
                () => response.AverageVehicleAgeByBrand = CalculateAverageVehicleAgeByBrand(),
                () => response.EngineUsePercentage = CalculateEngineUsePercentage(),
                () => response.DriverIdsWithRequiredEyeColorAndEngines = CalculateDriverIdsWithRequiredEyeColorAndEngines(),
                () => response.DriverIdsWithSingleTypeOfEngine = CalculateDriverIdsWithSingleTypeOfEngine(),
                () => response.WrongBrandIds = CalculateWrongBrandIds()
            );
            
            return response;
        }

        
        private bool IsBrandIdCorrect(int brandId)
        {
            return brandId > -1 && brandId < 10;
        }

        
        private int CalculateAge(DateTime birthDate)
        {
            int yearDiff = DateTime.Now.Year - birthDate.Year;
            if (DateTime.Now.DayOfYear < birthDate.DayOfYear)
            {
                return yearDiff - 1;
            }

            return yearDiff;
        }

        
        private int CalculateAverageAge()
        {
            _logger.LogInformation("Executing CalculateAverageAge");
            
            return (int) Math.Round(
                _driverProfiles
                .Select(driverProfile => CalculateAge(driverProfile.Driver.BirthDate))
                .Average()
            );
        }

        
        private Dictionary<int, int> CalculateDriverCountByYearOfBirth()
        {
            _logger.LogInformation("Executing CalculateDriverCountByYearOfBirth");

            Dictionary<int, int> result = _driverProfiles
                .Select(driverProfile => driverProfile.Driver.BirthDate.Year)
                .GroupBy(year => year)
                .ToDictionary(g => g.Key, g => g.Count());
            
            return result;
        }

        
        private Dictionary<string, List<long>> CalculateDriverIdsByLastNames()
        {
            _logger.LogInformation("Executing CalculateDriverIdsByLastNames");
            
            var result = new Dictionary<string, List<long>>();

            foreach (DriverProfile driver in _driverProfiles)
            {
                if (!result.ContainsKey(driver.Driver.LastName))
                {
                    result[driver.Driver.LastName] = new List<long>();
                }

                result[driver.Driver.LastName].Add(driver.Id);
            }

            result = result
                .Where(i => i.Value.Count > 2)
                .ToDictionary(t => t.Key, t => t.Value);

            return result.Keys.Count > 0 ? result : null;
        }
        
        
        private List<string> CalculateMostFrequentlyUsedBrands()
        {
            _logger.LogInformation("Executing CalculateMostFrequentlyUsedBrands");
            
            List<string> brands = _driverProfiles
                .SelectMany(driverProfile => driverProfile.VehicleInfo)
                .Where(vehicle => IsBrandIdCorrect(vehicle.BrandId))
                .GroupBy(vehicle => vehicle.BrandId)
                .OrderByDescending(g => g.Count())
                .Take(3)
                .Select(g => _brandIdToNameMap[g.Key]).ToList();
            
            return brands;
        }
        
        
        private Dictionary<string, double> CalculateAverageVehicleAgeByBrand()
        {
            _logger.LogInformation("Executing CalculateAverageVehicleAgeByBrand");
            
            var result = new Dictionary<string, double>();
            
            List<Vehicle> vehicleList = _driverProfiles
                .SelectMany(driverProfile => driverProfile.VehicleInfo)
                .Where(vehicle => IsBrandIdCorrect(vehicle.BrandId))
                .ToList();

            foreach (string brandName in _brandIdToNameMap.Values)
            {
                result[brandName] = vehicleList
                    .Where(vehicle => _brandIdToNameMap[vehicle.BrandId] == brandName)
                    .Select(vehicle => DateTime.Now.Year - vehicle.ModelYear)
                    .Average();
                result[brandName] = Math.Round(result[brandName], 2);
            }

            return result.Keys.Count > 0 ? result : null;
        }

        
        private Dictionary<string, double> CalculateEngineUsePercentage()
        {
            _logger.LogInformation("Executing CalculateEngineUsePercentage");

            List<Vehicle> vehicleList = _driverProfiles
                .SelectMany(driverProfile => driverProfile.VehicleInfo)
                .ToList();

            long total = vehicleList.Count;

            Dictionary<string, double> result = vehicleList
                .GroupBy(vehicle => vehicle.EngineType)
                .ToDictionary(g => g.Key, g => Math.Round((double) g.Count() / total * 100, 2));

            return result;
        }

        private bool HasRequiredEyeColorAndRequiredEngines(DriverProfile driverProfile)
        {
            string requiredEyeColor = "blue";
            var requiredEngineTypes = new HashSet<string> { "Hybrid", "Electric" };
            
            if (driverProfile.Driver.EyeColor == requiredEyeColor)
            {
                return driverProfile.VehicleInfo
                    .Count(vehicle => requiredEngineTypes.Contains(vehicle.EngineType)) > 0;
            }

            return false;
        }

        
        private List<long> CalculateDriverIdsWithRequiredEyeColorAndEngines()
        {
            _logger.LogInformation("Executing CalculateDriverIdsWithRequiredEyeColorAndEngines");
            
            return _driverProfiles
                .Where(HasRequiredEyeColorAndRequiredEngines)
                .Select(driverProfile => driverProfile.Id)
                .ToList();
        }


        private List<long> CalculateDriverIdsWithSingleTypeOfEngine()
        {
            _logger.LogInformation("Executing CalculateDriverIdsWithSingleTypeOfEngine");
            
            return _driverProfiles
                .Where(driverProfile => driverProfile.VehicleInfo.Count > 1)
                .Where(driverProfile => driverProfile.VehicleInfo
                    .Select(vehicle => vehicle.EngineType)
                    .ToHashSet().Count == 1)
                .Select(driverProfile => driverProfile.Id)
                .ToList();
        }

        private List<int> CalculateWrongBrandIds()
        {
            _logger.LogInformation("Executing CalculateWrongBrandIds");
            
            List<int> brandIdList = _driverProfiles
                .SelectMany(driverProfile => driverProfile.VehicleInfo)
                .Where(vehicle => !IsBrandIdCorrect(vehicle.BrandId))
                .Select(vehicle => vehicle.BrandId)
                .ToHashSet()
                .ToList();
            
            return brandIdList;
        }
    }
}