using System.Collections.Generic;

namespace TestTask.Models
{
    public class DriverProfile
    {
        public long Id { get; set; }
        public List<Vehicle> VehicleInfo { get; set; }
        public Driver Driver { get; set; }
    }
}