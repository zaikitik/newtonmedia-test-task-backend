using System.Collections.Generic;

namespace TestTask.Models
{
    public class DriverDataResponse
    {
        public int AverageAge { get; set; }
        public Dictionary<int, int> DriverCountByYearOfBirth { get; set; }
        public Dictionary<string, List<long>> DriverIdsByLastNames { get; set; }
        public List<string> MostFrequentlyUsedBrands { get; set; }
        public Dictionary<string, double> AverageVehicleAgeByBrand { get; set; }
        public Dictionary<string, double> EngineUsePercentage { get; set; }
        public List<long> DriverIdsWithRequiredEyeColorAndEngines { get; set; }
        public List<long> DriverIdsWithSingleTypeOfEngine { get; set; }
        public List<int> WrongBrandIds { get; set; }
    }
}