using System;

namespace TestTask.Models
{
    public class Driver
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EyeColor { get; set; }
        public string Company { get; set; }
        public string City { get; set; }
        public DateTime BirthDate { get; set; }
    }
}