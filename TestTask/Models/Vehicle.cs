namespace TestTask.Models
{
    public class Vehicle
    {
        public int BrandId { get; set; }
        public int ModelYear { get; set; } 
        public string EngineType { get; set; }
    }
}